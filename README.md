# Kubernetes Workshop
Lab 08: Running Pods as DaemonSets

---

## Instructions

### Ensure that your environment is clean

 - List existent deployments
```
kubectl get all
```

### Add a label to a cluster node

 - List the cluster nodes to get its names
```
kubectl get nodes
```

 - Add the label "app=logging-node" to the first node
```
kubectl label node <node-name> app=logging-node
```

 - See the node details
```
kubectl describe node <node-name>
```

### Deploy a daemonSet

 - Let's create a daemonset, but before take a look to the daemonset definition
```
curl https://gitlab.com/sela-aks-workshop/lab-08/raw/master/daemonset.yaml
```

- Create the deamonset
```
kubectl create -f https://gitlab.com/sela-aks-workshop/lab-08/raw/master/daemonset.yaml
```

 - List existent daemonsets
```
kubectl get daemonsets
```

### Add a label to the second node

 - Add the label "app=logging-node" to the second node
```
kubectl label node <node-name> app=logging-node
```

 - List existent daemonsets
```
kubectl get ds
```

 - Describe pods to ensure each pod is deployed in a different node
```
kubectl describe pods | grep Node:
```

### Remove a node label

 - List the cluster nodes to get its names
```
kubectl get nodes
```

 - Remove the label "app=logging-node" from one of the nodes 
```
kubectl label node <node-name> app-
```

 - List existent daemonsets
```
kubectl get ds
```

 - Describe the remain pod and check in which node it's running
```
kubectl describe pods | grep Node:
```

### Cleanup

 - List existent resources
```
kubectl get all
```

 - Delete existent resources
```
kubectl delete all --all
```

 - List existent resources
```
kubectl get all
```
